<?php 
$comdata=array('job_welfare'=>array('0'=>'93','1'=>'94','2'=>'92','3'=>'91','4'=>'90','5'=>'95','6'=>'96','7'=>'97','8'=>'98')
,'job_lang'=>array('0'=>'107','1'=>'108','2'=>'109','3'=>'110','4'=>'101','5'=>'100','6'=>'102','7'=>'104','8'=>'105','9'=>'106','10'=>'103')
,'job_age'=>array('0'=>'88','1'=>'85','2'=>'86','3'=>'87')
,'job_marriage'=>array('0'=>'72','1'=>'73','2'=>'112')
,'job_edu'=>array('0'=>'65','1'=>'66','2'=>'67','3'=>'68','4'=>'69','5'=>'70','6'=>'71')
,'job_sex'=>array('0'=>'62','1'=>'63','2'=>'64')
,'job_report'=>array('0'=>'57','1'=>'58','2'=>'59','3'=>'60','4'=>'61')
,'job_type'=>array('0'=>'54','1'=>'55','2'=>'56')
,'job_salary'=>array('0'=>'46','1'=>'47','2'=>'83','3'=>'48','4'=>'49','5'=>'50','6'=>'51','7'=>'52','8'=>'53')
,'job_pr'=>array('0'=>'79','1'=>'80','2'=>'81','3'=>'82','4'=>'20','5'=>'21','6'=>'22','7'=>'23','8'=>'24','9'=>'25')
,'job_etime'=>array('0'=>'134')
,'job_exp'=>array('0'=>'127','1'=>'12','2'=>'13','3'=>'14','4'=>'15','5'=>'16','6'=>'17','7'=>'18')
,'job_message'=>array('0'=>'77','1'=>'76','2'=>'78')
,'job_number'=>array('0'=>'40','1'=>'41','2'=>'42','3'=>'43','4'=>'44','5'=>'45')
,'job_mun'=>array('0'=>'27','1'=>'28','2'=>'29','3'=>'30','4'=>'31','5'=>'32')
)
; 
$comclass_name=array('89'=>'福利待遇','99'=>'语言要求','84'=>'年龄要求','65'=>'不限','93'=>'综合补贴','94'=>'年终奖金','40'=>'若干','39'=>'婚姻状况','38'=>'教育程度','37'=>'性别','36'=>'到岗时间','35'=>'工作性质','92'=>'包吃住','91'=>'五险一金','57'=>'不限','62'=>'不限','54'=>'不限','79'=>'上市公司','80'=>'国家机关','81'=>'事业单位','82'=>'其他','72'=>'不限','90'=>'三险一金','34'=>'薪水待遇','95'=>'奖励计划','96'=>'销售奖金','19'=>'企业性质','107'=>'西班牙语','108'=>'粤语','109'=>'闽南语','110'=>'上海话','133'=>'体验时间','10'=>'工作经验','75'=>'企业反馈类型','33'=>'招聘人数','20'=>'外资企业','101'=>'普通话二级','100'=>'普通话一级','27'=>'10人以下','97'=>'休假制度','26'=>'企业规模','102'=>'普通话三级','104'=>'韩语','105'=>'德语','106'=>'法语','98'=>'法定节假日','63'=>'男','66'=>'高中','73'=>'已婚','77'=>'咨询','134'=>'1-2天','127'=>'不限','88'=>'不限','76'=>'建议','55'=>'全职','46'=>'面议','41'=>'1-2人','28'=>'10-50人','58'=>'1周以内','21'=>'合资企业','47'=>'1000以下','85'=>'18-25岁','42'=>'3-4人','64'=>'女','29'=>'50-200人','78'=>'购买','59'=>'2周以内','56'=>'兼职','67'=>'中专','12'=>'应届毕业生','112'=>'未婚','22'=>'私营企业','60'=>'3周以内','43'=>'5-6人','23'=>'民营企业','83'=>'2000 - 2999','30'=>'200-500人','13'=>'1年以上','48'=>'1000 - 1999','68'=>'大专','86'=>'35岁以下','14'=>'2年以上','69'=>'本科','24'=>'股份制企业','25'=>'集体企业','87'=>'35岁以上','61'=>'1个月之内','44'=>'7-8人','49'=>'3000 - 4499','31'=>'500-1000人','15'=>'3年以上','50'=>'4500 - 5999','70'=>'硕士','32'=>'1000人以上','45'=>'9-10人','51'=>'6000 - 7999','71'=>'博士','16'=>'5年以上','17'=>'8年以上','52'=>'8000 - 9999','18'=>'10年以上','53'=>'10000及以上','103'=>'英语')
; 
?>