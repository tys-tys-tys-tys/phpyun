<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-11-06 11:24:09
         compiled from "E:\home\ftp\h\haoid\wwwroot\app\template\admin\admin_alipay_config.htm" */ ?>
<?php /*%%SmartyHeaderCode:14627581ea259476212-33375374%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c7390bb28a530ca5439d720a36410f699689be79' => 
    array (
      0 => 'E:\\home\\ftp\\h\\haoid\\wwwroot\\app\\template\\admin\\admin_alipay_config.htm',
      1 => 1467019310,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14627581ea259476212-33375374',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'config' => 0,
    'pytoken' => 0,
    'alipaydata' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_581ea2594a5027_95154241',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_581ea2594a5027_95154241')) {function content_581ea2594a5027_95154241($_smarty_tpl) {?><!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<link href="images/reset.css" rel="stylesheet" type="text/css" />
<link href="images/system.css" rel="stylesheet" type="text/css" /> 
<link href="images/table_form.css" rel="stylesheet" type="text/css" />
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['config']->value['sy_weburl'];?>
/js/jquery-1.8.0.min.js"><?php echo '</script'; ?>
> 
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['config']->value['sy_weburl'];?>
/js/layer/layer.min.js" language="javascript"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="js/admin_public.js" language="javascript"><?php echo '</script'; ?>
> 
<title>后台管理</title>
</head>
<?php echo '<script'; ?>
>
$(document).ready(function(){
	$(".input-text").focus(function(){ 
		var msg_id=$(this).attr('id');
		var msg=$('#'+msg_id+' + font').html(); 
		if($.trim(msg)!=''){
			layer.tips(msg, this, {
			guide: 1, 
			style: ['background-color:#5EA7DC; color:#fff;top:-7px', '#5EA7DC'], 
			}); 
			$(".xubox_layer").addClass("xubox_tips_border");
		} 
	}).blur(function () {
		layer.closeTips();
	});
});
<?php echo '</script'; ?>
>
<body class="body_ifm">
<div id="subboxdiv" style="width:100%;height:100%;display:none;position: absolute;"></div>
<div class="infoboxp">
<div class="infoboxp_top_bg"></div>
<div class="infoboxp_top">
<h6>支付宝设置</h6>
	<div class="infoboxp_right">
    	<a href="index.php?m=payconfig" class="infoboxp_tj">支付设置</a>
    </div>
</div>
<div class="main_tag">
	<div class="tag_box">
	<div>
<div id="right" style="display:block">
<iframe id="supportiframe"  name="supportiframe" onload="returnmessage('supportiframe');" style="display:none"></iframe>
	<form method="post" target="supportiframe" action="" name="config">
    <input type="hidden" name="pytoken" value="<?php echo $_smarty_tpl->tpl_vars['pytoken']->value;?>
">
	<div id="paysync">
	<table id="alipay" width="100%" class="table_form">
		<tr >
			<th width="200">合作身份者ID:</th>
			<td><input class="input-text" type="text" name="sy_alipayid" id="sy_alipayid" value="<?php echo $_smarty_tpl->tpl_vars['alipaydata']->value['sy_alipayid'];?>
" size="40" maxlength="255"/> <span class="admin_web_tip">如：208800200994****</span></td>
		</tr>
		<tr class="admin_table_trbg">
			<th width="200">安全检验码:</th>
			<td><input class="input-text" type="text" name="sy_alipaycode" id="sy_alipaycode" value="<?php echo $_smarty_tpl->tpl_vars['alipaydata']->value['sy_alipaycode'];?>
" size="45" maxlength="255"/><span class="admin_web_tip">如：7zzqkb09qu7zoo9ehny7j4sdfj22qqcf</span></td>
		</tr>
		<tr >
			<th width="200">签约支付宝账号或卖家支付宝帐户:</th>
			<td><input class="input-text" type="text" name="sy_alipayemail" id="sy_alipayemail" value="<?php echo $_smarty_tpl->tpl_vars['alipaydata']->value['sy_alipayemail'];?>
" size="40" maxlength="255"/></td>
		</tr>
        <tr class="admin_table_trbg">
			<th width="200">选择接口类型:</th>
			<td><select name="alipaytype" >
            <option value="1">即时到账交易</option>
			<option value="3"  <?php if ($_smarty_tpl->tpl_vars['config']->value['alipaytype']=='3') {?>selected<?php }?>>担保交易</option> 
            <option value="2"  <?php if ($_smarty_tpl->tpl_vars['config']->value['alipaytype']=='2') {?>selected<?php }?>>标准双接口</option> 
            </select><span class="admin_web_tip">请选择您最后一次跟支付宝签订的协议里面说明的接口类型</span></td>
		</tr>
		<tr >
			<th width="200">收款方名称:</th>
			<td><input class="input-text" type="text" name="sy_alipayname" id="sy_alipayname" value="<?php echo $_smarty_tpl->tpl_vars['alipaydata']->value['sy_alipayname'];?>
" size="40" maxlength="255"/> <span class="admin_web_tip">如：公司名称、网站名称、收款人姓名等</span> </td>
		</tr>
	</table>
	
	</div>
	<table width="100%" class="table_form">
		<tr class="admin_table_trbg">
			<td align="center" colspan="2"><input class="admin_submit4" id="pay_config" type="submit" name="pay_config" value="&nbsp; 修 改 &nbsp;"  />&nbsp;&nbsp;<input class="admin_submit4" type="reset" name="reset" value="&nbsp; 重 置 &nbsp;" /></td>
		</tr>
	</table>
	</form>
</div>
</div>
</div>
</body>
</html><?php }} ?>
